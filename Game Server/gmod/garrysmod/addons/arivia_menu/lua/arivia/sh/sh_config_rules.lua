Arivia.RulesText = [[
1.0 - RDM [Random Death Match] är ej tillåtet, this means you are not allowed to kill anyone without a valid RP reason.
Du får inte mörda någon utan en giltig RP anledning (Rollspel).


1.1 -- Respektera NLR [New Life Rule], anpassa dig utifrån ditt nya liv.
Du får inte åter-spawna och mörda någon som hämnd då varje liv räknas som ett nytt liv.
UNDANTAG:
	* Polisstyrkan: en konstapel kan begära förstärkning.
	* Gäng: kan även begära förstärkning vid ett gäng krig.

1.2 -- Under en rollplay aktig händelse får du ej byta liv för att fly undan situationen.
Med detta menas när du byter jobb är du fortfarande samma person.

1.3 -- Realistisk RP (FearRP), när någon siktar mot dig med ett vapen så måste du agera som i verkligheten.
Förväntingsvis ska du anpassa dig utifrån situationen och agera korrekt utifrån [1.1 och 1.0], när du dör har du ett nytt liv, respektera ditt nuvarande.


2.0 - Respektera din omgiving.
2.1 -- Använd chatten korrekt, sunt förnuft för det du säger.
2.2 -- Du får ej missbruka "sprej" funktionen för att sprida PG-18 innehåll.



3.0 - Lurendrejeri mot vapenförsäljare är ej tillåtet.
När du köper ett vapen måste du betala det korrekta priset.
3.1 -- Vapenförsäljaren får inte heller lura konsumenten.

3.1 - Självtillförsel är ej tillåtet, du får inte byta jobb för detta syfte


4.0 - "/OOC" skall endast används för "Out Of Character" diskussioner.
5.0 - "/Advert" för illigala handlingar, innan du gör dessa:
5.1 -- Mug.
5.1.1 --- Du får ej mugga mer än 3 000KR.
5.1.2 --- Mug får ej utnyttjas flertal gånger mot specifika personer.
5.2 -- Inbrott.
5.3 -- Bankrån.


6.0 - Alla jobb under kategorin "Regeringen" får ej genomföra vapen kontroll utan anledning.

6.1 -- Du får även ej döda dina medarbetare när du jobbar för "Regeringen".


6.2 -- Regeringsmedlemmar får ej öppna andras dörrar utan en /warrant


6.3 -- Du får ej bli Polis för att släppas loss häktade personer.


6.4 -- Regeringsmedlemmar får endast använda deras vapen när..
	* Någon är efterlyst.
	* Hotar ditt liv.
	* Utför illegala handlingar.

7.0 Fake "keypads" är ej tillåtna, alla måste vara fungerande.
]]