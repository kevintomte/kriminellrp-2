Arivia.UseIconsWithInfo = true
Arivia.MenuLinkDonate = "http://facepunch.com/"
Arivia.MenuLinkWebsite = "http://facepunch.com/"
Arivia.MenuLinkWorkshop = "http://facepunch.com/"
Arivia.InfoButtons = {
    {
        name = "PERSONAL",
        description = "TILL DIN TJÄNST!",
        icon = "arivia/arivia_btn_staff.png",
        buttonNormal = Color( 94, 75, 75, 190 ),
        buttonHover = Color( 94, 75, 75, 240 ),
        textNormal = Color(255, 255, 255, 255),
        textHover = Color(255, 255, 255, 255),
        enabled = true,
        func = function()
            Arivia:OpenAdmins()
        end
    },
    {
        name = "REGLER",
        description = "SAKER DU BÖR VETA",
		icon = "icons/info.png",
        buttonNormal = Color(72, 112, 58, 190),
        buttonHover = Color(72, 112, 58, 240),
        textNormal = Color(255, 255, 255, 255),
        textHover = Color(255, 255, 255, 255),
        enabled = true,
        func = function()
            Arivia:OpenRules(Arivia.RulesText)
        end
    },
    {
        name = "DONERA",
        description = "VARJE SLANT HJÄLPER :)",
        icon = "arivia/arivia_btn_donate.png",
        buttonNormal = Color(64, 105, 126, 190),
        buttonHover = Color(64, 105, 126, 240),
        textNormal = Color(255, 255, 255, 255),
        textHover = Color(255, 255, 255, 255),
        enabled = true,
        func = function()
            Arivia:OpenURL(Arivia.MenuLinkDonate)
        end
    },
    {
        name = "VÅR HEMSIDA",
        description = "BESÖK VÅR HEMSIDA",
        icon = "arivia/arivia_btn_website.png",
        buttonNormal = Color(163, 135, 79, 190),
        buttonHover = Color(163, 135, 79, 240),
        textNormal = Color(255, 255, 255, 255),
        textHover = Color(255, 255, 255, 255),
        enabled = true,
        func = function()
            Arivia:OpenURL(Arivia.MenuLinkWebsite)
        end
    }
}