TEAM_MEDBORGARE = DarkRP.createJob("Medborgare", {
	color = Color(123, 191, 106, 255),
	model = {
		"models/humans/group02/player/tale_08.mdl",
		"models/humans/group02/player/tale_09.mdl",
		"models/humans/group02/player/tale_05.mdl",
		"models/humans/group02/player/tale_04.mdl",
		"models/humans/group02/player/tale_03.mdl",
		"models/humans/group02/player/tale_01.mdl",
		"models/humans/group02/player/temale_01.mdl",
		"models/humans/group02/player/temale_02.mdl",
		"models/humans/group02/player/temale_07.mdl"
	},
	description = [[Medborgare är den mest grundläggande nivån i samhället förutom att vara en luffare som är en annan sak. Du har ingen specifik roll i stadslivet.]],
	weapons = {},
	command = "medborgare",
	max = 0,
	salary = 50,
	admin = 0,
	vote = false,
	hasLicense = false,
	candemote = false,
	category = "Civila Medborgare",
	sortOrder = 1,
})

TEAM_UTELIGGARE = DarkRP.createJob("Uteliggare", {
	color = Color(123, 191, 106, 255),
	model = "models/jessev92/player/l4d/m9-hunter.mdl",
	description = [[
		» Den lägsta samhällsklassen
		» Ingen respekterar dig och alla skrattar åt dig
		» Du har inget hem
		» Du måste tigga pengar.. "PLZ KORONA"!
		» Sjunga för alla som passerar för att få pengar..
		» Bygg din egna trä stuga någonstans i ett hörn eller hälst utanför någons dörr]],
	weapons = {"weapon_bugbait"},
	command = "uteliggare",
	max = 2,
	salary = 0,
	admin = 0,
	vote = false,
	hasLicense = false,
	candemote = false,
	hobo = true,
	category = "Civila Medborgare",
	sortOrder = 2,
})

TEAM_BANKDR = DarkRP.createJob("Bankdirektör", {
	color = Color(123, 191, 106, 255),
	model = {
		"models/player/suits/male_09_open_waistcoat.mdl",
		"models/player/suits/male_01_open_waistcoat.mdl",
		"models/player/suits/male_02_open_waistcoat.mdl",
		"models/player/suits/male_03_open_waistcoat.mdl",
		"models/player/suits/male_04_open_waistcoat.mdl",
		"models/player/suits/male_05_open_waistcoat.mdl",
		"models/player/suits/male_06_open_waistcoat.mdl",
		"models/player/suits/male_07_open_waistcoat.mdl",
		"models/player/suits/male_08_open_waistcoat.mdl"
	},
	description = [[Medborgare är den mest grundläggande nivån i samhället förutom att vara en luffare som är en annan sak. Du har ingen specifik roll i stadslivet.]],
	weapons = {"fas2_m1911"},
	command = "bankdirektör",
	max = 3,
	salary = 80,
	admin = 0,
	vote = false,
	hasLicense = false,
	candemote = false,
	category = "Civila Medborgare",
	sortOrder = 4,
})

TEAM_VF = DarkRP.createJob("Vapenförsäljare", {
	color = Color(123, 191, 106, 255),
	model = "models/enhanced_survivors/player/player_namvet.mdl",
	description = [[Den enda personen i samhället som kan importa vapen
		Du har närkontakt med poliser och maffian, var försiktig med olaglig vapen försäljning!]],
	weapons = {},
	command = "vapenförsäljare",
	max = 2,
	salary = 75,
	admin = 0,
	vote = false,
	hasLicense = false,
	category = "Civila Medborgare",
	sortOrder = 5,
})

TEAM_DOKTOR = DarkRP.createJob("Doktor", {
	color = Color(16, 78, 139, 255),
	model = "models/player/kleiner.mdl",
	description = [[
		With your medical knowledge you work to restore players to full health.
		Without a medic, people cannot be healed.
		Left click with the Medical Kit to heal other players.
		Right click with the Medical Kit to heal yourself.]],
	weapons = {"fas2_ifak"},
	command = "doktor",
	max = 3,
	salary = 89,
	admin = 0,
	vote = false,
	hasLicense = false,
	medic = true,
	category = "Beredskapstjänst",
	sortOrder = 6,
})

TEAM_BK = DarkRP.createJob("Brandkår", {
	color = Color(16, 78, 139, 255),
	model = "models/fearless/fireman2.mdl",
	description = [[
		With your medical knowledge you work to restore players to full health.
		Without a medic, people cannot be healed.
		Left click with the Medical Kit to heal other players.
		Right click with the Medical Kit to heal yourself.]],
	weapons = {},
	command = "brandkår",
	max = 3,
	salary = 84,
	admin = 0,
	vote = false,
	hasLicense = false,
	category = "Beredskapstjänst",
	sortOrder = 7,
})

TEAM_ORTEN = DarkRP.createJob("Orten", {
	color = Color(16, 16, 16, 255),
	model = "models/half-dead/gopniks/extra/playermodelonly.mdl",
	description = [[
		The lowest person of crime.
		A gangster generally works for the Mobboss who runs the crime family.
		The Mob boss sets your agenda and you follow it or you might be punished.]],
	weapons = {"csgo_default_t"},
	command = "orten",
	max = 0,
	salary = 40,
	admin = 0,
	vote = false,
	hasLicense = false,
	category = "Gäng",
	sortOrder = 8,
})

TEAM_ORTENSLEDARE = DarkRP.createJob("Ortens Ledare", {
	color = Color(16, 16, 16, 255),
	model = "models/half-dead/gopniks/extra/playermodelonly.mdl",
	description = [[
		The Mob boss is the boss of the criminals in the city.
		With his power he coordinates the gangsters and forms an efficient crime organization.
		He has the ability to break into houses by using a lockpick.
		The Mob boss posesses the ability to unarrest you.]],
	weapons = {"csgo_default_t", "fas2_m1911"},
	command = "ortens ledare",
	max = 1,
	salary = 56,
	admin = 0,
	vote = false,
	hasLicense = false,
	category = "Gäng",
	sortOrder = 9,
})

TEAM_AFA = DarkRP.createJob("AFA", {
	color = Color(79, 79, 79, 255),
	model = {
		"models/csgoanarchist2pm.mdl",
		"models/csgoanarchist3pm.mdl",
		"models/csgoanarchist1pm.mdl"
	},
	description = [[
		The Mob boss is the boss of the criminals in the city.
		With his power he coordinates the gangsters and forms an efficient crime organization.
		He has the ability to break into houses by using a lockpick.
		The Mob boss posesses the ability to unarrest you.]],
	weapons = {"fas2_p226"},
	command = "afa",
	max = 0,
	salary = 53,
	admin = 0,
	vote = false,
	hasLicense = false,
	category = "Gäng",
	sortOrder = 10,
})

TEAM_AFASLEDARE = DarkRP.createJob("AFAs Ledare", {
	color = Color(79, 79, 79, 255),
	model = "models/csgoanarchist4pm.mdl",
	description = [[
		The Mob boss is the boss of the criminals in the city.
		With his power he coordinates the gangsters and forms an efficient crime organization.
		He has the ability to break into houses by using a lockpick.
		The Mob boss posesses the ability to unarrest you.]],
	weapons = {"fas2_deagle"},
	command = "afas ledare",
	max = 1,
	salary = 59,
	admin = 0,
	vote = false,
	hasLicense = false,
	category = "Gäng",
	sortOrder = 11,
})

TEAM_SMR = DarkRP.createJob("SMR", {
	color = Color(34, 139, 34, 255),
	model = "models/humans/group02/player/tale_06.mdl",
	description = [[The Mob boss is the boss of the criminals in the city.
		With his power he coordinates the gangsters and forms an efficient crime organization.
		He has the ability to break into houses by using a lockpick.
		The Mob boss posesses the ability to unarrest you.]],
	weapons = {"fas2_p226"},
	command = "smr",
	max = 0,
	salary = 53,
	admin = 0,
	vote = false,
	hasLicense = false,
	category = "Gäng",
	sortOrder = 12,
})

TEAM_SMRLEDARE = DarkRP.createJob("SMRs Ledare", {
	color = Color(34, 139, 34, 255),
	model = "models/humans/group02/player/tale_07.mdl",
	description = [[The Mob boss is the boss of the criminals in the city.
		With his power he coordinates the gangsters and forms an efficient crime organization.
		He has the ability to break into houses by using a lockpick.
		The Mob boss posesses the ability to unarrest you.]],
	weapons = {"fas2_ots33"},
	command = "smrs ledare",
	max = 1,
	salary = 59,
	admin = 0,
	vote = false,
	hasLicense = false,
	category = "Gäng",
	sortOrder = 13,
})

TEAM_MAFFIA = DarkRP.createJob("Maffia", {
	color = Color(195, 0, 0, 255),
	model = {
		"models/humans/mafia/male_02.mdl",
		"models/humans/mafia/male_04.mdl",
		"models/humans/mafia/male_07.mdl",
		"models/humans/mafia/male_08.mdl",
		"models/humans/mafia/male_09.mdl"
	},
	description = [[
		The Mob boss is the boss of the criminals in the city.
		With his power he coordinates the gangsters and forms an efficient crime organization.
		He has the ability to break into houses by using a lockpick.
		The Mob boss posesses the ability to unarrest you.]],
	weapons = {"fas2_ragingbull"},
	command = "maffia",
	max = 0,
	salary = 63,
	admin = 0,
	vote = false,
	hasLicense = false,
	category = "Kriminella",
	sortOrder = 14,
})

TEAM_MAFFIALEDARE = DarkRP.createJob("Maffialedare", {
	color = Color(195, 0, 0, 255),
	model = "models/vito.mdl",
	description = [[
		The Mob boss is the boss of the criminals in the city.
		With his power he coordinates the gangsters and forms an efficient crime organization.
		He has the ability to break into houses by using a lockpick.
		The Mob boss posesses the ability to unarrest you.]],
	weapons = {"fas2_ragingbull", "fas2_uzi"},
	command = "maffialedare",
	max = 1,
	salary = 69,
	admin = 0,
	vote = false,
	hasLicense = false,
	category = "Kriminella",
	sortOrder = 15,
})

TEAM_KNARKLANGARE = DarkRP.createJob("Knarklangare", {
	color = Color(195, 0, 0, 255),
	model = "models/h-d/2sg/simonplayer.mdl",
	description = [[
		The Mob boss is the boss of the criminals in the city.
		With his power he coordinates the gangsters and forms an efficient crime organization.
		He has the ability to break into houses by using a lockpick.
		The Mob boss posesses the ability to unarrest you.]],
	weapons = {"csgo_falchion"},
	command = "knarklangare",
	max = 0,
	salary = 40,
	admin = 0,
	vote = false,
	hasLicense = false,
	category = "Kriminella",
	sortOrder = 16,
})

TEAM_AKOKARE = DarkRP.createJob("Amfetamin Kokare", {
	color = Color(195, 0, 0, 255),
	model = "models/dxn/cod_ghosts/hazmat_pm.mdl",
	description = [[
		The Mob boss is the boss of the criminals in the city.
		With his power he coordinates the gangsters and forms an efficient crime organization.
		He has the ability to break into houses by using a lockpick.
		The Mob boss posesses the ability to unarrest you.]],
	weapons = {},
	command = "amfetamin kokare",
	max = 2,
	salary = 40,
	admin = 0,
	vote = false,
	hasLicense = false,
	category = "Kriminella",
	sortOrder = 17,
})

TEAM_BANK = DarkRP.createJob("Bankrånare", {
	color = Color(195, 0, 0, 255),
	model = "models/player/suits/robber_open.mdl",
	description = [[
		The Mob boss is the boss of the criminals in the city.
		With his power he coordinates the gangsters and forms an efficient crime organization.
		He has the ability to break into houses by using a lockpick.
		The Mob boss posesses the ability to unarrest you.]],
	weapons = {},
	command = "bankrånare",
	max = 0,
	salary = 50,
	admin = 0,
	vote = false,
	hasLicense = false,
	category = "Kriminella",
	sortOrder = 18,
})

TEAM_BANKL = DarkRP.createJob("Bankrånare (Ledare)", {
	color = Color(195, 0, 0, 255),
	model = "models/player/suits/robber_shirt.mdl",
	description = [[
		The Mob boss is the boss of the criminals in the city.
		With his power he coordinates the gangsters and forms an efficient crime organization.
		He has the ability to break into houses by using a lockpick.
		The Mob boss posesses the ability to unarrest you.]],
	weapons = {"weapon_keypadchecker"},
	command = "bankrånare ledare",
	max = 1,
	salary = 50,
	admin = 0,
	vote = false,
	hasLicense = false,
	category = "Kriminella",
	sortOrder = 19,
})

TEAM_TJUV = DarkRP.createJob("Tjuv", {
	color = Color(195, 0, 0, 255),
	model = {
		"models/csgopheonix4pm.mdl",
		"models/csgopheonix2pm.mdl",
		"models/csgopheonix1pm.mdl",
		"models/csgopheonix3pm.mdl"
	},
	description = [[
		The Mob boss is the boss of the criminals in the city.
		With his power he coordinates the gangsters and forms an efficient crime organization.
		He has the ability to break into houses by using a lockpick.
		The Mob boss posesses the ability to unarrest you.]],
	weapons = {"lockpick", "weapon_keypadchecker"},
	command = "tjuv",
	max = 0,
	salary = 40,
	admin = 0,
	vote = false,
	hasLicense = false,
	category = "Kriminella",
	sortOrder = 20,
})

TEAM_SM = DarkRP.createJob("Seriemördare", {
	color = Color(195, 0, 0, 255),
	model = "models/player/slow/rorschach.mdl",
	description = [[
		The Mob boss is the boss of the criminals in the city.
		With his power he coordinates the gangsters and forms an efficient crime organization.
		He has the ability to break into houses by using a lockpick.
		The Mob boss posesses the ability to unarrest you.]],
	weapons = {"csgo_bayonet"},
	command = "seriemördare",
	max = 2,
	salary = 0,
	admin = 0,
	vote = false,
	hasLicense = false,
	category = "Kriminella",
	sortOrder = 21,
})

TEAM_KONSTAPEL = DarkRP.createJob("Konstapel", {
	color = Color(16, 78, 139, 255),
	model = {"models/gta5/player/citycoppm.mdl"},
	description = [[
		The protector of every citizen that lives in the city.
		You have the power to arrest criminals and protect innocents.
		Hit a player with your arrest baton to put them in jail.
		Bash a player with a stunstick and they may learn to obey the law.
		The Battering Ram can break down the door of a criminal, with a warrant for their arrest.
		The Battering Ram can also unfreeze frozen props (if enabled).
		Type /wanted <name> to alert the public to the presence of a criminal.]],
	weapons = {
		"arrest_stick",
		"unarrest_stick",
		"fas2_glock20",
		"stunstick",
		"door_ram",
		"weaponchecker"
	},
	command = "konstapel",
	max = 6,
	salary = 80,
	admin = 0,
	vote = false,
	hasLicense = true,
	category = "Regeringen",
	sortOrder = 22,
})

TEAM_KONSTAPELDONOR = DarkRP.createJob("Förstärkt Konstapel", {
	color = Color(16, 78, 139, 255),
	model = {"models/gta5/player/armoredcitycoppm.mdl"},
	description = [[
		The protector of every citizen that lives in the city.
		You have the power to arrest criminals and protect innocents.
		Hit a player with your arrest baton to put them in jail.
		Bash a player with a stunstick and they may learn to obey the law.
		The Battering Ram can break down the door of a criminal, with a warrant for their arrest.
		The Battering Ram can also unfreeze frozen props (if enabled).
		Type /wanted <name> to alert the public to the presence of a criminal.]],
	weapons = {
		"arrest_stick",
		"unarrest_stick",
		"fas2_glock20",
		"fas2_ks23",
		"stunstick",
		"door_ram",
		"weaponchecker"
	},
	command = "förstärkt konstapel",
	max = 6,
	salary = 86,
	admin = 0,
	vote = false,
	hasLicense = true,
	category = "Regeringen",
	sortOrder = 23,
})

TEAM_INSATSSTYRKA = DarkRP.createJob("Insatsstyrka", {
	color = Color(16, 78, 139, 255),
	model = {"models/gta5/player/swatpm.mdl"},
	description = [[
		The protector of every citizen that lives in the city.
		You have the power to arrest criminals and protect innocents.
		Hit a player with your arrest baton to put them in jail.
		Bash a player with a stunstick and they may learn to obey the law.
		The Battering Ram can break down the door of a criminal, with a warrant for their arrest.
		The Battering Ram can also unfreeze frozen props (if enabled).
		Type /wanted <name> to alert the public to the presence of a criminal.]],
	weapons = {
		"arrest_stick",
		"unarrest_stick",
		"fas2_glock20",
		"fas2_m4a1",
		"stunstick",
		"door_ram",
		"weaponchecker"
	},
	command = "insatsstyrka",
	max = 6,
	salary = 90,
	admin = 0,
	vote = false,
	hasLicense = true,
	category = "Regeringen",
	sortOrder = 24,
})

TEAM_INSATSSTYRKADONOR = DarkRP.createJob("Taktisk Insatsstyrka", {
	color = Color(16, 78, 139, 255),
	model = {"models/gta5/player/swatpm.mdl"},
	description = [[
		The protector of every citizen that lives in the city.
		You have the power to arrest criminals and protect innocents.
		Hit a player with your arrest baton to put them in jail.
		Bash a player with a stunstick and they may learn to obey the law.
		The Battering Ram can break down the door of a criminal, with a warrant for their arrest.
		The Battering Ram can also unfreeze frozen props (if enabled).
		Type /wanted <name> to alert the public to the presence of a criminal.]],
	weapons = {
		"arrest_stick",
		"unarrest_stick",
		"fas2_glock20",
		"fas2_m3s90",
		"fas2_m82",
		"stunstick",
		"door_ram",
		"weaponchecker"
	},
	command = "taktisk insatsstyrka",
	max = 6,
	salary = 96,
	admin = 0,
	vote = false,
	hasLicense = true,
	category = "Regeringen",
	sortOrder = 25,
})

TEAM_KOMMISSARIE = DarkRP.createJob("Kommissarie", {
	color = Color(16, 78, 139, 255),
	model = "models/gta5/player/deputypm.mdl",
	description = [[
		The Chief is the leader of the Civil Protection unit.
		Coordinate the police force to enforce law in the city.
		Hit a player with arrest baton to put them in jail.
		Bash a player with a stunstick and they may learn to obey the law.
		The Battering Ram can break down the door of a criminal, with a warrant for his/her arrest.
		Type /wanted <name> to alert the public to the presence of a criminal.
		Type /jailpos to set the Jail Position]],
	weapons = {
		"arrest_stick",
		"unarrest_stick",
		"fas2_ragingbull",
		"fas2_ks23",
		"stunstick",
		"door_ram",
		"weaponchecker"
	},
	command = "kommissarie",
	max = 4,
	salary = 87,
	admin = 0,
	vote = false,
	hasLicense = true,
	chief = true,
	NeedToChangeFrom = TEAM_KONSTAPEL,
	category = "Regeringen",
	sortOrder = 26,
})

TEAM_BM = DarkRP.createJob("Borgmästare", {
	color = Color(72, 130, 20, 255),
	model = {
		"models/player/tfa_irons.mdl",
		"models/player/tfa_irons_playercolor.mdl",
		"models/player/tfa_irons_casual.mdl"
	},
	description = [[
		The Mayor of the city creates laws to govern the city.
		If you are the mayor you may create and accept warrants.
		Type /wanted <name>  to warrant a player.
		Type /jailpos to set the Jail Position.
		Type /lockdown initiate a lockdown of the city.
		Everyone must be inside during a lockdown.
		The cops patrol the area.
		/unlockdown to end a lockdown]],
	weapons = {"fas2_ragingbull"},
	command = "borgmästare",
	max = 1,
	salary = 100,
	admin = 0,
	vote = true,
	hasLicense = false,
	mayor = true,
	category = "Regeringen",
	sortOrder = 27,
})

TEAM_TA = DarkRP.createJob("Tillgänglig Admin", {
	color = Color(255, 114, 22, 255),
	model = "models/player/combine_super_soldier.mdl",
	description = [[admin]],
	weapons = {},
	command = "tillgängligadmin",
	max = 0,
	salary = 0,
	admin = 2,
	vote = false,
	hasLicense = false,
	category = "Annat",
	sortOrder = 28,
})

TEAM_TM = DarkRP.createJob("Tillgänglig Moderator", {
	color = Color(255, 114, 22, 255),
	model = "models/player/combine_soldier_prisonguard.mdl",
	description = [[admin]],
	weapons = {},
	command = "tillgängligmoderator",
	max = 0,
	salary = 0,
	admin = 1,
	vote = false,
	hasLicense = false,
	category = "Annat",
	sortOrder = 29,
})
GAMEMODE.DefaultTeam = TEAM_MEDBORGARE
GAMEMODE.CivilProtection = {
	[TEAM_KONSTAPEL] = true,
	[TEAM_KONSTAPELDONOR] = true,
	[TEAM_INSATSSTYRKADONOR] = true,
	[TEAM_INSATSSTYRKADONOR] = true,
	[TEAM_KOMMISSARIE] = true,
	[TEAM_BM] = true,
}
DarkRP.addHitmanTeam(TEAM_SM)