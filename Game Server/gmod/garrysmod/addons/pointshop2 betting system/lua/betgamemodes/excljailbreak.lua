--Pointshop betting gamemode scripts are SHARED. Hooks must return the same data on both server and client.
--This script is for the Jailbreak gamemode by Excl (Jailbreak 7)

--Gamemode scripts MUST ALWAYS specify the first and second teams to bet on with these settings:
BETTING.FirstTeam = TEAM_GUARD
BETTING.SecondTeam = TEAM_PRISONER
//You could use this file to load custom settings based on the gamemode e.g.
//BETTING.Settings.MinimumPlayersForBetting = 5

local function PlayerCanBetJailbreak(ply)
	--BETTING.Settings.OnlyAllowBettingAtRoundStartTime enabled
	if tonumber(BETTING.Settings.OnlyAllowBettingAtRoundStartTime) and BETTING.Settings.OnlyAllowBettingAtRoundStartTime > 0 then
		if BETTING.EndBetsTime and CurTime() <= BETTING.EndBetsTime then return true end
		return false,string.format("You must place a bet in the first %i seconds of the round!",BETTING.Settings.OnlyAllowBettingAtRoundStartTime)
	end
	
	--BETTING.Settings.OnlyAllowBettingAtRoundStartTime disabled
	if ply:Alive() then return false,"Players must be dead or spectating to bet." end
	if JB.State != STATE_SETUP and JB.State != STATE_PLAYING then return false,"You cant bet whilst a new round is preparing." end
	return true
end
hook.Add("PlayerCanBet","PlayerCanBetJailbreak",PlayerCanBetJailbreak)

local function AllowBetsWhenRoundBegins()
	BETTING.EndBetsTime = CurTime() + BETTING.Settings.OnlyAllowBettingAtRoundStartTime or 30
end
hook.Add("JailBreakRoundStart","AllowBetsWhenRoundBegins",AllowBetsWhenRoundBegins)

--SERVER hooks
--For some reason the JailBreakRoundEnd hook doesn't send the winner argument, so we override EndRound
--Every outcome must call BETTING.FinishBets else the bet window will not be closed
if SERVER then
	local OldEndRound = JB.EndRound
	local function ExclJailbreakEndRoundBets(gm,winner)
		OldEndRound(gm,winner)
		if not winner then return end
		if (winner == 0) then BETTING.FinishBets(2, true)
		else BETTING.FinishBets(winner) end
	end
	JB.EndRound = ExclJailbreakEndRoundBets
end